 **晨风公装网---电销系统**
 
    在Honestead下开发 配置环境
    ~/Homestead/Homestead.yaml 配置环境
    ip: "192.168.10.10"
    memory: 2048
    cpus: 1
    provider: virtualbox
    
    authorize: ~/.ssh/id_rsa.pub
    
    keys:
        - ~/.ssh/id_rsa
    
    #本地和虚拟机共享的项目目录
    folders:
        - map: ~/code
          to: /home/vagrant/code
    
    sites:
        - map: shop.test
          to: /home/vagrant/code/qie-shop/public
    
    databases:
        - homestead
        - qie-shop
    
    # blackfire:
    #     - id: foo
    #       token: bar
    #       client-id: foo
    #       client-token: bar
    
    # ports:
    #     - send: 50000
    #       to: 5000
    #     - send: 7777
    #       to: 777
    #       protocol: udp


    着重这几项
    sites:
        - map: shop.test
          to: /home/vagrant/Code/qie-shop/public
          .
          .
          .
    databases:
        - homestead
        - qie-shop
        
        
        
**导入配置 并启动登录**
        
        此项是为了把homestead.yaml 文件排配置同步
        vagrant reload --provision
        cd ~/Homestead && vagrant provision && vagrant up
        vagrant ssh
        
**此后操作都在虚拟机进行**

**切换镜像**
    
    阿里云镜像 composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
    composer create-project laravel/laravel laravel-shop --prefer-dist "5.5.*"

**运行 Laravel Mix**

    Yarn 配置安装加速：
    yarn config set registry https://registry.npm.taobao.org
    使用 Yarn 安装依赖：
    SASS_BINARY_SITE=http://npm.taobao.org/mirrors/node-sass yarn

**监听css、js**

    npm run watch-poll
    watch-poll 会在你的终端里持续运行，监控 resources 文件夹下的资源文件是否有发生改变。在 watch-poll 命令运行的情况下，一旦资源文件发生变化，Webpack 会自动重新编译。
    
    若出现错误cross-env 找不到 则
    win10环境下
    
    第零步（如果遇到错误的时候执行，否则请跳过）：
    rm -rf node_modules
    
    第一步（国内网络环境原因，必须修改）：国内环境
    yarn config set registry  https://registry.npm.taobao.org
    
    第二步（一定要加上 -- no-bin-links）：
    yarn install --no-bin-links
    
    第三步修改项目根目录下的 package.json 文件：
    "scripts":{
    "dev": "npm run development",
    "development": "NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
    "watch": "NODE_ENV=development node_modules/webpack/bin/webpack.js --watch --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
    "watch-poll": "npm run watch -- --watch-poll",
    "hot": "NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --config=node_modules/laravel-mix/setup/webpack.config.js",
    "prod": "npm run production"
    "production": "NODE_ENV=production node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js"
    },
    
    运行 npm run watch-poll
    

**项目开发记录**
   
1.认证脚手架命令，即可生成认证，修改，找回密码等功能。

`php artisan make:auth`

2.发送邮件验证用户是否机会成功，给user表增加一个字段

`php artisan make:migration users_add_email_verified --table=users`

3.生成中间件，增加路由，去http/middleware/kernel.php  路由注册

4.为没注册用户直接点击激活邮件

`php artisan make:notification EmailVerificationNotification`

5.邮件激活逻辑处理

`php artisan make:controller EmailVerificationController`




